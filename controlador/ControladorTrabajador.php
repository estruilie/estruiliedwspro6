<?php
if (strlen(session_id()) < 1)
    session_start();
error_reporting(E_ALL);
ini_set('display_errors','1');

include_once "funciones.php";
include_once "../modelo/ModeloFichero.php";

function mostrarTrabajadores() {
    if ( $_SESSION['modelo']==1 )
        $modelo = new ModeloFichero();
    else $modelo = new ModeloMysql();
    $trabajadores = $modelo->leerTrabajadores();

    if ( count($trabajadores) > 0 ) {

        foreach ( $trabajadores as $t ) {
            echo "<tr>";
            echo "<td>" . $t->getId() . "</td>";
            echo "<td>" . $t->getNombre() . "</td>";
            echo "<td>" . $modelo->nombreDepartamentoPorId($t->getDepartamento()) . "</td>";
            echo "</tr>";
        }
    }
}

function guardarTrabajador() {
    if ( isset($_REQUEST["id"]) && isset($_REQUEST["nombre"]) ) {
        $id = recoge("id");
        $nombre = recoge("nombre");
        $departamento = recoge("departamento");

        $trabajador = new Trabajador($id, $nombre, $departamento);

        if ( $_SESSION['modelo']==1 )
            $modelo = new ModeloFichero();
        else
            $modelo = new ModeloMysql();
        $modelo->guardarTrabajador($trabajador);

        header("Location: ../vista/VistaOperacionOK.php");
    }
}

function nuevoIdTra() {
    if ( $_SESSION['modelo']==1 )
        $modelo = new ModeloFichero();
    else
        $modelo = new ModeloMysql();
    
    $todos = $modelo->leerTrabajadores();
    $id = count($todos) + 1;
    echo $id;
}

function rellenarCombo() {
    if ( $_SESSION['modelo']==1 )
        $modelo = new ModeloFichero();
    else
        $modelo = new ModeloMysql();
    $departamentos = $modelo->leerDepartamentos();
    if ( $departamentos ) {
        foreach ($departamentos as $d) {
            echo "<option value='" . $d->getId()."'>" . $d->getNombre() . "</option>";
        }
    }
    else
        echo "<option value=''>Todavía no existe ningún departamento</option>";
}

?>





