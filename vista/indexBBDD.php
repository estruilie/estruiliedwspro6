<?php 
    require "../modelo/config.php"; 
    require "../controlador/funciones.php";
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Opciones BBDD</title>
        <link rel="stylesheet" href="../css/index.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        <div class="cuerpo"></div>
        <div class="centrar">
        <h1>Escoge una opción para entrar en la aplicación con el modelo BBDD:</h1><hr/><br/>
            <form method="POST" action="#" onsubmit="<?php opcionBBDD();?>" >
                <input type="radio" name="instalar" value="1" checked="checked" >Instalar la BBDD
                <br/>
                <input type="radio" name="instalar" value="2" >Entrar en la aplicación (BBDD previamente instalada)
                <br/><br/><br/>
                <input type="submit" name="enviar" value="Continuar" />
                <br/>
                 
            </form>
       </div>
        <?php include "footer.php"; ?>
    </body>
</html>