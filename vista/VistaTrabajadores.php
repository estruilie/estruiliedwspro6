<?php 
    require "../modelo/config.php";
    include("../controlador/ControladorTrabajador.php");
    error_reporting(E_ALL ^ E_NOTICE);
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>trabajadores.php</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
   
        <div class="tablas">
            <h2> TRABAJADORES EXISTENTES: </h2>
            
            <table border="2">
            <tr>
            <th>ID Trabajador</th>
            <th>Nombre Trabajador</th>
            <th>Departamento Asociado</th>
            </tr>

            <?php mostrarTrabajadores(); ?>    

            </table>
            <br/>
               
            
        </div>
        <br/><h2><a href="VistaAltaTrabajador.php"> Dar de alta un nuevo trabajador </a></h2>
        
        <?php include "footer.php"; ?>
    </body>
</html>