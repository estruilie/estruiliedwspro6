<?php 
    require "../modelo/config.php";
    include ("../controlador/ControladorTrabajador.php");
    error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Añadir trabajador</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        
        <h2>Introduzca los datos del nuevo trabajador</h2>
        <form method="POST"  action="#" onsubmit="<?php guardarTrabajador();?>" >
            
            <label for="id">Identificador:</label>
            <input size="4" type="text" name="id" readonly value="<?php nuevoIdTra()?>"/>
            <br/><br/>

            <label for="nombre">Nombre:</label>
            <input size="50" type="text" name="nombre" required placeholder="Nombre y apellidos" title="Por favor, introduce un nombre correcto. Sólo letras, espacios y comas" pattern="[a-zA-Z,\W]+" />
            <br/><br/>
            
            <label for="departamento">Departamento:</label>
            <select name="departamento">
                <?php rellenarCombo();?>
            </select>
            <br/><br/><br/><hr/><br/><br/>
            
            <input type="submit" name="Enviar" value="Enviar" />
            <input type="reset" name="Borrar" value="Borrar" />
            <br/><br/>

        </form>
        
        <?php include "footer.php"; ?>
    </body>
</html>