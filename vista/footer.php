<?php
if (strlen(session_id()) < 1)
    session_start();

/* 
 * PIE DE PÁGINA INCLUÍDO EN TODAS LAS PÁGINAS
 */

function inicioModelo() {
    // se utiliza para redirigir a la página de inicio del modelo en caso de encontrarse en una distinta

    // $_SERVER['PHP_SELF'] devuelve ruta actual. Pej: /dwst5p1/vista/index.php
    $ini = strpos($_SERVER['PHP_SELF'],"index"); //devuelve la posición de la cadena buscada. False si no encontrada

    // redirigimos a la página de inicio del modelo correspondiente
    if ( $ini == false) {
        if ( $_SESSION['modelo'] == 1 )
            echo "<a href = 'index2.php'> &gt;&gt; Inicio ficheros</a>";
        else
            echo "<a href = 'index2.php'> &gt;&gt; Inicio BBDD</a>";
    }
}

function inicioAplicacion() {
    // se utiliza para redirigir a la página de inicio de la aplicación en caso de encontrarse en una distinta

    $expReg = "/index\.php/";
    // $_SERVER['PHP_SELF'] devuelve ruta actual. Pej: /dwst5p1/vista/index.php
     //devuelve la posición de la cadena buscada. False si no encontrada

    // redirigimos a la página de inicio de la aplicación
    if ( !preg_match($expReg,$_SERVER['PHP_SELF']) ) 
        echo "<a href = 'index.php'> &gt;&gt; Inicio aplicación</a>";
    
}

echo "<div class='pie1'>";
echo "<p>" . Config::$autor . " - " . Config::$curso . "</p>";
echo "<p>" . Config::$empresa . " - " . Config::$fecha . " - <a href='../doc/Documentacion.pdf' target='_blank' >Ver Documentacion.pdf</a></p>";
echo "</div>";
echo "<div class='pie2'>";
    inicioModelo();
echo "</div>";
echo "<div class='pie3'>";
    inicioAplicacion();
echo "</div>";
?>