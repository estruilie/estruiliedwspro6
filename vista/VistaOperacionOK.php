<?php 
    require "../modelo/config.php";
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Operación ejecutada</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
        
        <h2>La operación se ha ejecutado correctamente</h2>
        
        <?php include "footer.php"; ?>
    </body>
</html>