<?php 
    require "../modelo/config.php";
    include("../controlador/ControladorDepartamento.php");
    error_reporting(E_ALL ^ E_NOTICE);
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>departamentos.php</title>
        <link rel="stylesheet" href="../css/index.css">
        <link rel="stylesheet" href="../css/estilos.css">
    </head>
    <body>
        <?php include "header.php"; ?>
   
        <div class="tablas">
            <h2> DEPARTAMENTOS EXISTENTES: </h2>
            <table border="2">
            <tr>
            <th>ID Departamento</th>
            <th>Nombre</th>
            <th>Ubicación</th>
            </tr>

            <?php mostrarDepartamentos(); ?>

            </table>
            <br/>
                
            
        </div>
        <br/><h2><a href="VistaAltaDepartamento.php"> Dar de alta un nuevo departamento </a></h2>
        
        <?php include "footer.php"; ?>
    </body>
</html>